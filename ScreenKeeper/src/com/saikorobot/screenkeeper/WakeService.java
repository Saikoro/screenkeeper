package com.saikorobot.screenkeeper;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.util.Log;

public class WakeService extends Service {

    static final String TAG = "WakeService";

    public static final String ACTION_SCREEN_LOCK_START = "com.saikorobot.screenkeeper.intent.SCREEN_LOCK_START";
    public static final String ACTION_SCREEN_LOCK_RELEASE = "com.saikorobot.screenkeeper.intent.SCREEN_LOCK_RELEASE";

    PowerManager mPm;
    WakeLock mWakeLock;

    @SuppressWarnings("deprecation")
    @Override
    public void onCreate() {
	super.onCreate();

	mPm = (PowerManager) getSystemService(Context.POWER_SERVICE);
	mWakeLock = mPm.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK
		| PowerManager.ACQUIRE_CAUSES_WAKEUP, TAG);
	Log.i(TAG, "onCreate");
    }

    @Override
    public void onStart(Intent intent, int startId) {
	String act = intent.getAction();
	Log.i(TAG, "onStart act=" + act);
	if (ACTION_SCREEN_LOCK_START.equals(act)) {
	    startLock();
	} else if (ACTION_SCREEN_LOCK_RELEASE.equals(act)) {
	    stopLock();
	} else {
	    stopSelf();
	}
    }

    @SuppressWarnings("deprecation")
    private void startLock() {
	try {
	    mWakeLock.acquire();
	    Log.i(TAG, "startLock");
	    NotificationManager nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
	    Notification n = new Notification();
	    n.icon = R.drawable.ic_stat_name;
	    n.tickerText = getString(R.string.wake_service_start);
	    n.when = System.currentTimeMillis();
	    n.flags = Notification.FLAG_ONGOING_EVENT
		    | Notification.FLAG_NO_CLEAR;
	    Intent service = new Intent(this, WakeService.class);
	    service.setAction(WakeService.ACTION_SCREEN_LOCK_RELEASE);
	    PendingIntent pi = PendingIntent.getService(this, 0, service, 0);
	    n.setLatestEventInfo(getApplicationContext(),
		    getString(R.string.app_name),
		    getString(R.string.service_running_tap_to_stop), pi);
	    nm.notify(0, n);
	} catch (Exception e) {
	    e.printStackTrace();
	}
    }

    private void stopLock() {
	try {
	    if (mWakeLock != null && mWakeLock.isHeld()) {
		Log.i(TAG, "stopLock");
		mWakeLock.release();
	    }
	    NotificationManager nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
	    nm.cancelAll();
	} catch (Exception e) {
	    e.printStackTrace();
	}
    }

    @Override
    public IBinder onBind(Intent intent) {
	return null;
    }

}
