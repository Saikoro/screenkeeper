package com.saikorobot.screenkeeper;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.widget.CompoundButton;
import android.widget.ToggleButton;

public class MainActivity extends Activity implements
	CompoundButton.OnCheckedChangeListener {

    ToggleButton mBtnActive;
    SharedPreferences mSp;

    @Override
    public void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.activity_main);

	mSp = PreferenceManager
		.getDefaultSharedPreferences(getApplicationContext());

	mBtnActive = (ToggleButton) findViewById(R.id.btn_enable);
	mBtnActive.setChecked(mSp.getBoolean("service_active", false));
	mBtnActive.setOnCheckedChangeListener(this);

    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
	switch (buttonView.getId()) {
	case R.id.btn_enable:
	    mSp.edit().putBoolean("service_active", isChecked).commit();
	    if (isChecked) {
		Intent service = new Intent(this, WakeService.class);
		service.setAction(WakeService.ACTION_SCREEN_LOCK_START);
		startService(service);
	    } else {
		Intent service = new Intent(this, WakeService.class);
		service.setAction(WakeService.ACTION_SCREEN_LOCK_RELEASE);
		startService(service);
	    }
	    break;

	default:
	    break;
	}
    }

}
