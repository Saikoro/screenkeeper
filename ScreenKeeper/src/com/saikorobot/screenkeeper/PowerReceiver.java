package com.saikorobot.screenkeeper;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

public class PowerReceiver extends BroadcastReceiver {

    static final String TAG = "PowerReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {
	String act = intent.getAction();
	SharedPreferences pref = PreferenceManager
		.getDefaultSharedPreferences(context);
	boolean active = pref.getBoolean("service_active", false);
	Log.i(TAG, "onReceive action=" + act + " flag=" + active);
	if (!active)
	    return;

	if (Intent.ACTION_POWER_CONNECTED.equals(act)) {
	    Intent service = new Intent(context, WakeService.class);
	    service.setAction(WakeService.ACTION_SCREEN_LOCK_START);
	    context.startService(service);
	} else if (Intent.ACTION_POWER_DISCONNECTED.equals(act)) {
	    Intent service = new Intent(context, WakeService.class);
	    service.setAction(WakeService.ACTION_SCREEN_LOCK_RELEASE);
	    context.startService(service);
	}
    }
}
